var _WM_APP_PROPERTIES = {
  "activeTheme" : "mobile",
  "defaultLanguage" : "en",
  "displayName" : "SpaceIssue",
  "homePage" : "Main",
  "name" : "SpaceIssue",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};